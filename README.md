# DR.BENCH

This is the pipeline for DR.BENCH: **D**iagnostic **R**easoning **Bench**mark for clinical natural language processing. The benchmark and preliminary result is described in our paper: *DR.BENCH: Diagnostic Reasoning Benchmark for Clinical Natural Language Processing*.

The paper is accepted to Journal of Biomedical Informatics. Please cite us if you are using DR.BENCH: 

**Gao, Yanjun, Dmitriy Dligach, Timothy Miller, John Caskey, Brihat Sharma, Matthew M. Churpek, and Majid Afshar. "DR. BENCH: Diagnostic Reasoning Benchmark for Clinical Natural Language Processing." Journal of Biomedical Informatics 138 (2023): 104286.**

Preprint is available at [https://arxiv.org/abs/2209.14901](https://arxiv.org/abs/2209.14901) for those without subscription access to the Journal of Biomedical Informatics.

## Description
DR.BENCH is composed of six tasks from five existing publications with publicly available datasets. It is built upon our previous investigation of clinical NLP tasks that facilitate model development for clinical diagnostic reasoning. In our previous work, we designed a hierarchical annotation framework that follows the cognitive workflow of physicians reviewing SOAP format daily progress note, conducted and published three stages of annotation that correspond to three clinical NLP tasks: SOAP Section labeling (SOAP Labeling), Assessment and Plan Relation Labeling (AP) and Problem List Summarization (Summ).  

![Introduction of DR.BENCH](./drbench_intro.png)

## Dataset 
We built a Progress Note Understanding, a suite of cNLP tasks for diagnostic reasoning, which includes the SOAP Labeling task, the AP task, and the Summ task. Data and annotation is available at PhysioNet: [Link](https://www.physionet.org/content/task-1-3-soap-note-tag/1.0.0/). The annotation is described in our [LREC Paper](https://aclanthology.org/2022.lrec-1.587/).  You should be able to find the rest of the tasks through their original papers. 

Please cite this paper if you are using any task from Progress Note Understanding: 

Yanjun Gao, Dmitriy Dligach, Timothy Miller, Samuel Tesch, Ryan Laffin, Matthew M. Churpek, and Majid Afshar. 2022. Hierarchical Annotation for Building A Suite of Clinical Natural Language Processing Tasks: Progress Note Understanding. In Proceedings of the Thirteenth Language Resources and Evaluation Conference, pages 5484–5493, Marseille, France. European Language Resources Association. 

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

### Step 0: Download datasets
- Progress Note Understanding (SOAP Section Labeling, Summarization): https://www.physionet.org/content/task-1-3-soap-note-tag/1.0.0/ 
- emrQA: https://github.com/panushri25/emrQA
- MedQA: https://drive.google.com/file/d/1ImYUSLk9JbgHXOemfvyiDiirluZHPeQw/view 
- medNLI: https://physionet.org/content/mednli/1.0.0/ 
- Assessment and Plan Relation Labeling: N2C2 Website (TBA) 

### Step 1: Running DR.BENCH pipeline

```
python drbench.py 
```
This script comes with the following arguments: 
![DR.BENCH script arguments](./dbench_args.png)

For example, if you want to run a T5-Base vanilla model on MedNLI, SOAP, Summarization task, you could run:
```
python drbench.py --i mednli soap summ --model_path t5-base  
```

## Contributing
Y.G, D.D, T.M, M.C, and M.A conceived and planned the experiments. Y.G, J.C, B.S, and M.A prepared codes and carried out the experiments. Y.G, D.D, T.M, M.C, and M.A contributed to the interpretation of the results. Y.G and M.A took the lead in writing the manuscript. All authors provided critical feedback and helped shape the research, analysis and manuscript.

## Authors and acknowledgment
Yanjun Gao (ICU Data Science Lab, UW-Madison),  Dmitry Dligach (Loyola University Chicago), Timothy Miller (Boston Children's Hospital and Harvard Medical School), John Caskey, Brihat Sharma, Matthew M.Churpek and Majid Afshar (all from ICU Data Science Lab, UW-Madison). 

## License
TBD

## Contact
ygao@medicine.wisc.edu 
