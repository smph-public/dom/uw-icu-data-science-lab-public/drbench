"""
Author: ygao
"""
import os 
from transformers import T5Tokenizer, T5ForConditionalGeneration, AdamW
import torch
import torch.nn as nn
import torch.nn.init
from torch.autograd import Variable
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.backends.cudnn as cudnn
from torch.nn.utils.clip_grad import clip_grad_norm
import numpy as np
import datetime 
import csv
from collections import OrderedDict
from summ_dataset import * 
import logging 
import argparse 
from tqdm import tqdm 
from rouge_score import rouge_scorer

class Summarizer(nn.Module):
	def __init__(self, model, tokenizer, evaluator, data_base_path, summ_type, config, exp_name, finetune_path):
		super(Summarizer, self).__init__()
		self.tokenizer = tokenizer 
		self.model = model 
		self.metric = None  
		self.ft_path = finetune_path
		#self.device = torch.device(config.device)
		self.exp_name = exp_name
		self.evaluator = evaluator
		self.LR = config.get("LR", 1e-5)
		self.batch_size = config.get("batch_size", 8)
		self.device = config.get("device","cpu")
		self.epoch = config.get("epoch",1)
		self.summ_type = summ_type
		self.adam_epsilon = config.get("adam_epsilon", 0.99)
		self.LR_scheduler = config.get("LR_scheduler", None) 
		self.weight_decay = config.get("weight_decay", 0.99) 
		self.beam_size = 10
		self.prefix = "summarize: "
		self.data_base_path = data_base_path
		self.init_dataset()
		self.create_optimizers()


	def init_dataset(self):
		"""
		Construct DataLoader for Training and Validation 
		"""
		self.tokenizer.add_tokens(self.prefix) 
		self.tokenizer.add_tokens("<ASSESSMENT>")
		self.tokenizer.add_tokens("<SUBJECTIVE>")
		self.model.resize_token_embeddings(len(self.tokenizer)) 

		train_df = self.data_base_path+"train.csv"
		val_df = self.data_base_path+"val.csv"
		test_df = self.data_base_path+"test.csv"  
		summ_dataset = NoteSummary(train_df, val_df, test_df, self.tokenizer, self.prefix, self.summ_type)
		self.model.to(self.device)
		self.train_loader, self.val_loader, self.test_loader = summ_dataset.get_data_loaders(self.batch_size)
		logging.info(f"==== TASKS: ProblemSumm ==== \n Data Base Path: {self.data_base_path}, Exp Name: {self.exp_name}, LR: {self.LR}")	


	def create_optimizers(self):
		no_decay = ["bias", "LayerNorm.weight"]
		optimizer_grouped_parameters = [
		    {
		        "params": [p for n, p in self.model.named_parameters() if not any(nd in n for nd in no_decay)],
		        "weight_decay": self.weight_decay,
		    },
		    {
		        "params": [p for n, p in self.model.named_parameters() if any(nd in n for nd in no_decay)],
		        "weight_decay": 0.0,
		    },
		]
		print(f"LR: {self.LR}")
		if type(self.LR) == float:
			self.optimizer = torch.optim.Adam(optimizer_grouped_parameters, lr=self.LR)
		else:
			self.optimizer = torch.optim.Adam(optimizer_grouped_parameters, lr=self.LR[0])
		self.lr_scheduler = None # change to use scheduler later 

	def optimizer_step(self):
		self.optimizer.step()
		self.optimizer.zero_grad()

	def forward_per_batch(self, batch):
		"""
		Input: batch sample with input ids, input mask, target ids, target mask 
		"""
		src, src_mask, tgt, tgt_mask = batch 
		lm_labels = tgt.to(self.device) # target encoding input ids 
		lm_labels[lm_labels[:, :] == self.tokenizer.pad_token_id] = -100

		outputs = self.model(
		    input_ids=src.squeeze(1).to(self.device),
		    attention_mask=src_mask.squeeze(1).to(self.device),
		    labels = lm_labels.squeeze(1),
		    decoder_attention_mask=tgt_mask.squeeze(1).to(self.device)
		)

		loss = outputs[0]

		return loss 


	def train(self, num_steps_per_validate=64): 
		"""
		Complete forward pass on all training samples
		Input: 
		train_data: training set dataloader (iterable)
		val_data: validation set dataloader (iterable)
		"""
		avg_loss = []
		cnt = 0 

		val_scs, val_lo = [], [] 
		#for batch in tqdm(train_data,unit="batch",desc="Training"):
		early_stop = False
		self.optimizer.zero_grad()
		batch_cnt = 0 
		best_model = self.model 
		self.max_rl = 0
		for ep in range(self.epoch): 
			for batch in self.train_loader:
				batch_cnt+=1 
				batch_loss = self.forward_per_batch(batch)
				batch_loss.backward()
				self.optimizer_step()
				avg_loss.append(batch_loss.item())
				cnt +=1 
				if cnt != 0 and cnt % num_steps_per_validate == 0:
					print("STEP {} TRAIN AVG LOSS {}".format(cnt, np.mean(avg_loss)))
					self.model.eval()
					val_loss, r2, rl = self.validate()
					ep_val_info = "STEP {} VALIDATION AVG LOSS {} ROUGE-2 {:.2F} ROUGE-L {:.2F}".format(cnt, val_loss, r2*100, rl*100)
					print(ep_val_info)
					logging.info(ep_val_info)
					val_scs.append(rl)
					val_lo.append(val_loss) # already a numpy number 
					if rl > self.max_rl:
						self.max_rl = rl 
						logging.info("FOUND BEST ROUGE-L {} AT STEP {}".format(self.max_rl, cnt))
						best_model = self.model 

					if len(val_scs) > 3:
						if val_scs[-2] > val_scs[-1] and val_scs[-3] > val_scs[-2]: # validation performance from two consecutive epochs are decreasing 
							logging.info("Early Stop Trigger at Step {} ".format(cnt))
							print("Early Stop Trigger at Step {} ".format(cnt))
							early_stop = True 
							break # early stopping 
	 
						if val_lo[-3] < val_lo[-2] and val_lo[-2] < val_lo[-1]:
							logging.info("Early Stop Trigger at Step {} ".format(cnt))
							print("Early Stop Trigger at Step {} ".format(cnt))
							early_stop = True
							break # early stopping 
				self.model.train()

		return best_model

	def validate(self):
		"""
		Default validation on single instance basis 
		"""
		val_loss = [] 
		avg_r2, avg_rl = [], [] 
		batch_cnt = 0
		with torch.no_grad():
			for batch in tqdm(self.val_loader,unit="batch",desc="Validating"):
				#if batch_cnt >2 :
				#	continue 
				batch_cnt += 1 
				batch_loss = self.forward_per_batch(batch).detach() 
				val_loss.append(batch_loss.item())
				decode_seq, scores, gts = self.decode(batch) 
				avg_r2.append(scores['rouge2'].fmeasure)
				avg_rl.append(scores['rougeL'].fmeasure)

		return np.mean(val_loss), np.mean(avg_r2), np.mean(avg_rl)

	def decode(self,batch,metrics=["Rouge"]):
		"""
		For validation or inference: 
		call T5 to generate tokens 
		"""
		src, src_mask, tgt, tgt_mask = batch 
		generate_ids = self.model.generate(input_ids=src.squeeze(1).to(self.device),                    
								attention_mask=src_mask.squeeze(1).to(self.device),
			                    max_length=128,
								num_beams=self.beam_size,
								repetition_penalty=1,
								no_repeat_ngram_size=2)
		preds = [self.tokenizer.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=True) for g in generate_ids] 
		self.batch = batch 
		gts = [self.tokenizer.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=True) for g in tgt[0]]
		r_scores = None 
		b_scores = None 
		if "Rouge" in metrics:
			r_scores = self.applyPyRouge(gts[0], preds[0])
		if "BERTScore" in metrics:
			b_scores = self.evaluator.applyBERTScore(gts[0], preds[0]) 

		return preds, r_scores, gts  

	def applyPyRouge(self, gts, preds):
		scorer = rouge_scorer.RougeScorer(['rouge1','rouge2','rougeL'])
		scores = scorer.score(gts, preds) 
		return scores 


	def test(self, best_model):
		avg_r2, avg_rl = [], [] 
		
		batch_cnt = 0 
		outname=self.ft_path+"Summ_"+self.summ_type+"_test.csv" 
		pred_seqs, gold_seqs = [], []
		with torch.no_grad(): 
			for batch in tqdm(self.test_loader,unit="batch",desc="Testing"):
				#if batch_cnt > 2:
				#	continue
				batch_cnt +=1 
				decode_seq, scores, gt_seq = self.decode(batch) 
				avg_r2.append(scores['rouge2'].fmeasure)
				avg_rl.append(scores['rougel'].fmeasure)
				pred_seqs.append(decode_seq)
				gold_seqs.append(gt_seq)

		with open(outname, "w") as outf:
			wr = csv.writer(outf)
			header = ["Prediction","Ground Truth"]
			wr.writerow(header)
			for item in zip(pred_seqs, gold_seqs):
				wr.writerow([item[0], item[1]])

		test_report = f"Summarization Input type {self.summ_type}, Test Result on All Problem Summaries:\n  R2: {np.mean(avg_r2):.4f} RL: {np.mean(avg_rl):.4f}"
		print(test_report)
		logging.info(test_report)

		return pred_seqs, gold_seqs


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Problem Summ Config Path")

	parser.add_argument('--pretrain_name', dest="inputprefix",type=str,default="T5-Base-Vanilla")

	parser.add_argument('--lr', dest="lr",type=float, default=1e-5)
	parser.add_argument('--device', dest="device", type=str, default="cuda")
	parser.add_argument('--epoch',dest="epoch",type=int,default=8)

	parser.add_argument('--model_path', dest="model_base_path", type=str, default="t5-base")
	parser.add_argument('--pretrain_state_dict', dest="pretrained",type=str,default="")
	parser.add_argument('--tokenizer_path', dest="tokenizer_path", type=str, default="t5-base")
	parser.add_argument('--data_path', dest="data_base_path", type=str, default="../Data/Reasoning/Summ/")
	parser.add_argument('--summ_type', dest="summ_type", type=str, default="All")
	#parser.add_argument('--device', dest="device", type=str, default="cuda")


	args = parser.parse_args() 


	exp_name = args.inputprefix
	model_base_path = args.model_base_path 
	tokenizer_path = args.tokenizer_path
	data_base_path = args.data_base_path 
	device_name = args.device
	EPOCHS  = args.epoch 
	pretrained_state_dict = args.pretrained  
	lr = args.lr 
	epochs = args.epoch 
	summ_type = args.summ_type

	configs = {"LR": lr, 
				"epoch": EPOCHS,
				"device": device_name, 
				"batch_size":4,
	}

	tokenizer = T5Tokenizer.from_pretrained(model_base_path)
	model = T5ForConditionalGeneration.from_pretrained(model_base_path)

	if pretrained_state_dict != "":
		model.load_state_dict(torch.load(pretrained_state_dict))
	exp_prefix = datetime.datetime.now().strftime('%d-%m-%y-%H-%M') 
	finetune_path = "../Finetune/"+exp_name+"-Summ"

	device = torch.device(device_name)

	finetune_path += "-".join(exp_prefix.split("-")[:3])+"/"

	if not os.path.isdir(finetune_path):
		os.makedirs(finetune_path)
		print("==== Creating Folder for Experiment ====")

	logging.basicConfig(filename=finetune_path+"train.log", level=logging.INFO)
	exp_setting = f"LR: {lr}\t Epochs: {EPOCHS}\t Pretrained Type: {exp_name}\t"

	now = datetime.datetime.now()
	dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
	print("TRAINER CREATED: date and time =", dt_string)	
	logging.info("TRAINER CREATED: date and time = {}".format(str(dt_string))) 

	logging.info(exp_setting)
	
	#model.resize_token_embeddings(len(tokenizer))
	from summ_evaluation import *
	setting = {} 
	evaluator = Evaluator(setting)
	trainer = Summarizer(model, tokenizer, evaluator, data_base_path, summ_type, configs, exp_name, finetune_path)
	best_model = trainer.train()
	pred_seqs, gold_seqs = trainer.test(best_model)



