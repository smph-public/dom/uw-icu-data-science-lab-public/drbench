"""
Author: ygao
"""
import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import pickle
import os
from transformers import BertTokenizer
import pandas as pd 

class MNLIDataGen(Dataset):

	def __init__(self, train_df, val_df, test_df, tokenizer, prefix="INFERENCE: "):
		self.label_dict = {'entailment': 0, 'contradiction': 1, 'neutral': 2}

		self.train_df = train_df
		self.val_df = val_df
		self.test_df = test_df

		self.base_path = ''
		#self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True) # Using a pre-trained BERT tokenizer to encode sentences
		self.tokenizer = tokenizer
		self.train_data = None
		self.val_data = None
		self.prefix = prefix
		self.init_data()

	def init_data(self):
		self.train_data = self.load_data(self.train_df)
		self.val_data = self.load_data(self.val_df)
		self.test_data = self.load_data(self.test_df) 

	def load_data(self, df):
		MAX_LEN = 512
		token_ids = []
		mask_ids = []
		seg_ids = []
		y = []

		premise_list = df['Premise'].to_list()
		hypothesis_list = df['Hypothesis'].to_list()
		label_list = df['Relation'].to_list()

		for (premise, hypothesis, label) in zip(premise_list, hypothesis_list, label_list):
			pair_inputs = self.prefix + " <PREMISE> "+ premise + " <HYPOTHESIS> " + hypothesis 
			pair_encoding = self.tokenizer(pair_inputs)
			pair_input_ids = pair_encoding.input_ids
			attention_mask_ids = pair_encoding.attention_mask

			token_ids.append(torch.tensor(pair_input_ids))
			mask_ids.append(torch.tensor(attention_mask_ids))

			gold_label = self.tokenizer.encode(label)
			y.append(torch.tensor(gold_label))
		
		token_ids = pad_sequence(token_ids, batch_first=True)
		mask_ids = pad_sequence(mask_ids, batch_first=True)
		y = pad_sequence(y, batch_first=True) 
		dataset = TensorDataset(token_ids, mask_ids, y)
		print(len(dataset))
		return dataset

	def get_data_loaders(self, batch_size=32, shuffle=True):
		train_loader = DataLoader(
			self.train_data,
			shuffle=shuffle,
			batch_size=batch_size
		)

		val_loader = DataLoader(
			self.val_data,
			shuffle=shuffle,
			batch_size=batch_size
		)

		test_loader = DataLoader(
			self.test_data,
			shuffle=False,
			batch_size=1)

		return train_loader, val_loader, test_loader


if __name__ == "__main__":
	from transformers import AutoTokenizer
	tok = AutoTokenizer.from_pretrained("t5-base")
	
	train_df = pd.read_csv("mednli/train.csv")
	val_df = pd.read_csv("mednli/dev.csv")
	test_df = pd.read_csv("mednli/test.csv")

	mnli_dataset = MNLIDataGen(train_df, val_df, test_df, tok)