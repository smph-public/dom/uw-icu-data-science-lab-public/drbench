"""
Author: ygao
"""
import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import pickle
import os
from transformers import BertTokenizer
import pandas as pd 
import json 

class MEDQAGen(Dataset):

	def __init__(self, train_json, val_json, test_json, tokenizer, data_base_path=None, open_book=True, need_prefix=False, prefix=None):
		self.label_dict = {'A': 0, 'B': 1, 'C': 2, 'D':3, 'E':4}

		self.train_json = train_json
		self.val_json = val_json
		self.test_json = test_json 
		self.base_path = ''
		#self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True) # Using a pre-trained BERT tokenizer to encode sentences
		self.tokenizer = tokenizer
		self.train_data = None
		self.val_data = None
		self.need_prefix = need_prefix
		self.prefix = prefix
		self.open_book = open_book
		if self.open_book:
			self.train_cjson = data_base_path + "train_context.json"
			self.val_cjson = data_base_path + "dev_context.json"
			self.test_cjson = data_base_path + "test_context.json"
			print(f"Open book context under the path: train: {self.train_cjson}\t dev: {self.val_cjson}\t test: {self.test_cjson}\t") 

		self.init_data()

	def init_data(self):
		if self.open_book:
			self.train_data = self.load_data_open(self.train_json, self.train_cjson)
			self.val_data = self.load_data_open(self.val_json, self.val_cjson)
			self.test_data = self.load_data_open(self.test_json, self.test_cjson) 
		else:
			self.train_data = self.load_data_closed(self.train_json)
			self.val_data = self.load_data_closed(self.val_json)
			self.test_data = self.load_data_closed(self.test_json)

	def load_data_closed(self, df):
		MAX_LEN = 512
		token_ids = []
		mask_ids = []
		target_masks = []
		y = []
		answer_text = [] 

		with open(df,"r") as json_file:
			data = list(json_file)
			for record in data:
				item = json.loads(record)
				question, options, answer = item["question"], item["options"], item["answer_idx"]

				input_tks =  question  
				if self.need_prefix:
					input_tks = self.prefix + " <QUESTION> " + question + " <A> " + options['A'] + " <B> " + options['B'] + " <C> " + options['C'] + " <D> " + options['D'] + " <E> " + options['E']

				seqs_tks = self.tokenizer(input_tks, add_special_tokens=True, max_length=512, truncation=True)
				seqs_ids = torch.tensor(seqs_tks["input_ids"])
				attention_mask_ids = torch.tensor(seqs_tks["attention_mask"])

				answer_tks = self.tokenizer("<"+answer+">", add_special_tokens=True, max_length=10, truncation=True)
				answer_ids = torch.tensor(answer_tks["input_ids"])
				answer_masks = torch.tensor(answer_tks["attention_mask"])

				token_ids.append(seqs_ids)
				mask_ids.append(attention_mask_ids)

				y.append(answer_ids) 
				target_masks.append(answer_masks)
				answer_text.append(answer)
	
		token_ids = pad_sequence(token_ids, batch_first=True)
		mask_ids = pad_sequence(mask_ids, batch_first=True)
		target_labels = pad_sequence(y, batch_first=True)
		target_masks = pad_sequence(target_masks, batch_first=True)
		#y = torch.tensor(y)
		dataset = TensorDataset(token_ids, mask_ids, target_labels, target_masks)
		print("Closed book QA: ", len(dataset))
		return dataset

	def load_data_open(self, df, cjson):
		MAX_LEN = 512
		token_ids = []
		mask_ids = []
		target_masks = []
		y = []
		answer_text = [] 

		with open(cjson,"r") as json_file:
			context = json.load(json_file) 

		with open(df,"r") as json_file:
			data = list(json_file)
			for record in data:
				item = json.loads(record)
				question, options, answer = item["question"], item["options"], item["answer"]
				if question not in context:
					continue 
				con_tks = " ".join(context[question])
				#input_tks = question + " [SEP] " + con_tks 
				if self.need_prefix:
					input_tks = self.prefix + " <QUESTION> " + question + " <A> " + options['A'] + " <B> " + options['B'] + " <C> " + options['C'] + " <D> " + options['D'] + " <E> " + options['E'] + " <CONTEXT> " + con_tks

				seqs_tks = self.tokenizer(input_tks, add_special_tokens=True, max_length=512, truncation=True)
				seqs_ids = torch.tensor(seqs_tks["input_ids"])
				attention_mask_ids = torch.tensor(seqs_tks["attention_mask"])

				answer_tks = self.tokenizer(answer, add_special_tokens=True, max_length=512, truncation=True)
				answer_ids = torch.tensor(answer_tks["input_ids"])
				answer_masks = torch.tensor(answer_tks["attention_mask"])

				token_ids.append(seqs_ids)
				mask_ids.append(attention_mask_ids)

				y.append(answer_ids) 
				target_masks.append(answer_masks)
				answer_text.append(answer)
	
		token_ids = pad_sequence(token_ids, batch_first=True)
		mask_ids = pad_sequence(mask_ids, batch_first=True)
		target_labels = pad_sequence(y, batch_first=True)
		target_masks = pad_sequence(target_masks, batch_first=True)
		#y = torch.tensor(y)
		dataset = TensorDataset(token_ids, mask_ids, target_labels, target_masks)
		print("Open book QA: ", len(dataset))
		return dataset


	def get_data_loaders(self, batch_size=32, shuffle=True):
		train_loader = DataLoader(
		  self.train_data,
		  shuffle=shuffle,
		  batch_size=batch_size
		)

		val_loader = DataLoader(
		  self.val_data,
		  shuffle=shuffle,
		  batch_size=batch_size
		)

		test_loader = DataLoader(
		  self.test_data,
		  shuffle=False,
		  batch_size=1 
		)

		return train_loader, val_loader, test_loader
