"""
Author: ygao
"""
from medqagen_dataset import *
from transformers import AutoTokenizer, T5ForConditionalGeneration, AdamW
import time
from tqdm import tqdm
import argparse 
from evaluation import Evaluator 

"""
parser = argparse.ArgumentParser(description="MEDQA Generation Fine-tuning Config")
#yaml_path = "yamls/t5.yaml" uncomment this line if run under command line

parser.add_argument('--lr', dest="lr",type=float, default=1e-4)
parser.add_argument('--exp_name', dest="inputprefix",type=str,default="Default")
parser.add_argument('--model_path', dest="model_base_path", type=str, default="")
parser.add_argument('--tokenizer_path', dest="tokenizer_path", type=str, default="")
parser.add_argument('--data_path', dest="data_base_path", type=str, default="")
parser.add_argument('--device', dest="device", type=str, default="cuda")
parser.add_argument('--epoch',dest="epoch",type=int,default=5)


args = parser.parse_args() 

lr = args.lr 
exp_name = args.inputprefix
model_base_path = args.model_base_path 
tokenizer_path = args.tokenizer_path
data_base_path = args.data_base_path 
device_name = args.device
EPOCHS  = args.epoch 

"""
evaluator = Evaluator({})

model_base_path = "t5-base"

pretrained_state_dict = "../PLM/T5-Def-TB-20-07-22-02-55/t5.pth"
finetune_path = "../Finetune/t5-TB+DEF-MEDQA/t5.pth"
import os 
if os.path.isdir(finetune_path):
	pass
else:
	os.makedirs(finetune_path)

data_base_path = "../Data/Reasoning/MedQA/questions/"

device_name = "cuda"
EPOCHS = 15 
exp_name = "t5-TB+DEF"
lr = 1e-5
open_book = False 
run_test = False
beam_size = 5
tokenizer = AutoTokenizer.from_pretrained(model_base_path)
model = T5ForConditionalGeneration.from_pretrained(model_base_path)



need_prefix=True
prefix = "answer the question: "
tokenizer.add_tokens(prefix) 
tokenizer.add_tokens("<A1>")
tokenizer.add_tokens("<A2>")
tokenizer.add_tokens("<A3>")
tokenizer.add_tokens("<A4>")
tokenizer.add_tokens("<A5>")
tokenizer.add_tokens("<QUESTION>")

if open_book:
	tokenizer.add_tokens("<CONTEXT>")

#model.resize_token_embeddings(len(tokenizer))
model.load_state_dict(torch.load(pretrained_state_dict))
model.resize_token_embeddings(len(tokenizer))

medqa_dataset = MEDQAGen(data_base_path+"train.jsonl", data_base_path+"dev.jsonl", data_base_path+"test.jsonl", tokenizer, data_base_path=data_base_path, open_book=open_book, need_prefix=need_prefix, prefix=prefix)
train_loader, val_loader, test_loader = medqa_dataset.get_data_loaders(batch_size=8)

device = torch.device(device_name)

model.to(device)

param_optimizer = list(model.named_parameters())
no_decay = ['bias', 'gamma', 'beta']
optimizer_grouped_parameters = [
	{'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
	 'weight_decay_rate': 0.01},
	{'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
	 'weight_decay_rate': 0.0}
]

optimizer = AdamW(optimizer_grouped_parameters, lr=lr, correct_bias=False)
 
start = time.time()

total_test_acc = 0
with torch.no_grad():
	for batch in tqdm(test_loader):
		pair_token_ids, mask_ids, y, y_mask = batch
		optimizer.zero_grad()
		pair_token_ids = pair_token_ids.to(device)
		mask_ids = mask_ids.to(device)
		labels = y.to(device)

		outputs = model.generate(input_ids=pair_token_ids,                    					attention_mask=mask_ids,
							max_length=128,
							num_beams=beam_size,
							repetition_penalty=1,
							no_repeat_ngram_size=2)

		for b in range(0,y.shape[0]):
			pred_labels = tokenizer.decode(outputs[b], skip_special_tokens=True)
			gt_labels = tokenizer.decode(labels[b], skip_special_tokens=True)
			if pred_labels == gt_labels: 
				total_val_acc += 1

test_acc  = total_test_acc/len(test_loader)

print(f"Model {exp_name} Test Acc: {test_acc:.4f}")
