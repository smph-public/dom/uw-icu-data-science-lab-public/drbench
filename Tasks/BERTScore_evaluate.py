import torch 
import numpy as np 

from transformers import AutoTokenizer, AutoModel
import csv 
from sklearn.metrics.pairwise import cosine_similarity

"""
default_world = World(filename="pym.sqlite3")
PYM = default_world.get_ontology("http://PYM/").load()
CUI = PYM["CUI"]
max_source_length = 512
matcher = QuickUMLS("/Users/ygao/Downloads/2021AB/yanjun",overlapping_criteria="score",similarity_name="jaccard", threshold=1.0)
"""
tokenizer = AutoTokenizer.from_pretrained("cambridgeltl/SapBERT-from-PubMedBERT-fulltext")
model = AutoModel.from_pretrained("cambridgeltl/SapBERT-from-PubMedBERT-fulltext")

def greedy_cos_idf(ref_embedding, ref_masks, ref_idf, hyp_embedding, hyp_masks, hyp_idf, all_layers=False):
	"""
	Compute greedy matching based on cosine similarity.

	Args:
		- :param: `ref_embedding` (torch.Tensor):
				   embeddings of reference sentences, BxKxd,
				   B: batch size, K: longest length, d: bert dimenison
		- :param: `ref_lens` (list of int): list of reference sentence length.
		- :param: `ref_masks` (torch.LongTensor): BxKxK, BERT attention mask for
				   reference sentences.
		- :param: `ref_idf` (torch.Tensor): BxK, idf score of each word
				   piece in the reference setence
		- :param: `hyp_embedding` (torch.Tensor):
				   embeddings of candidate sentences, BxKxd,
				   B: batch size, K: longest length, d: bert dimenison
		- :param: `hyp_lens` (list of int): list of candidate sentence length.
		- :param: `hyp_masks` (torch.LongTensor): BxKxK, BERT attention mask for
				   candidate sentences.
		- :param: `hyp_idf` (torch.Tensor): BxK, idf score of each word
				   piece in the candidate setence
	"""
	ref_embedding.div_(torch.norm(ref_embedding, dim=-1).unsqueeze(-1))
	hyp_embedding.div_(torch.norm(hyp_embedding, dim=-1).unsqueeze(-1))

	if all_layers:
		B, _, L, D = hyp_embedding.size()
		hyp_embedding = hyp_embedding.transpose(1, 2).transpose(0, 1).contiguous().view(L * B, hyp_embedding.size(1), D)
		ref_embedding = ref_embedding.transpose(1, 2).transpose(0, 1).contiguous().view(L * B, ref_embedding.size(1), D)
	batch_size = ref_embedding.size(0)
	sim = torch.bmm(hyp_embedding, ref_embedding.transpose(1, 2))
	masks = torch.bmm(hyp_masks.unsqueeze(2).float(), ref_masks.unsqueeze(1).float())
	if all_layers:
		masks = masks.unsqueeze(0).expand(L, -1, -1, -1).contiguous().view_as(sim)
	else:
		masks = masks.expand(batch_size, -1, -1).contiguous().view_as(sim)

	masks = masks.float().to(sim.device)
	sim = sim * masks

	word_precision = sim.max(dim=2)[0]
	word_recall = sim.max(dim=1)[0]

	hyp_idf.div_(hyp_idf.sum(dim=1, keepdim=True))
	ref_idf.div_(ref_idf.sum(dim=1, keepdim=True))
	precision_scale = hyp_idf.to(word_precision.device)
	recall_scale = ref_idf.to(word_recall.device)
	if all_layers:
		precision_scale = precision_scale.unsqueeze(0).expand(L, B, -1).contiguous().view_as(word_precision)
		recall_scale = recall_scale.unsqueeze(0).expand(L, B, -1).contiguous().view_as(word_recall)
	P = (word_precision * precision_scale).sum(dim=1)
	R = (word_recall * recall_scale).sum(dim=1)
	F = 2 * P * R / (P + R)

	hyp_zero_mask = hyp_masks.sum(dim=1).eq(2)
	ref_zero_mask = ref_masks.sum(dim=1).eq(2)

	if all_layers:
		P = P.view(L, B)
		R = R.view(L, B)
		F = F.view(L, B)

	if torch.any(hyp_zero_mask):
		print(
			"Warning: Empty candidate sentence detected; setting raw BERTscores to 0.", file=sys.stderr,
		)
		P = P.masked_fill(hyp_zero_mask, 0.0)
		R = R.masked_fill(hyp_zero_mask, 0.0)

	if torch.any(ref_zero_mask):
		print("Warning: Empty reference sentence detected; setting raw BERTScores to 0.", file=sys.stderr)
		P = P.masked_fill(ref_zero_mask, 0.0)
		R = R.masked_fill(ref_zero_mask, 0.0)

	F = F.masked_fill(torch.isnan(F), 0.0)

	return P, R, F

def SapbertEval(orig, pred):
	encoding = tokenizer([orig, pred], padding='longest', max_length=128,truncation=True, return_tensors='pt')

	input_ids, attention_mask = encoding.input_ids, encoding.attention_mask
	output = model(input_ids=input_ids, attention_mask=attention_mask)


	#cosine_similarity(output['last_hidden_state'][0].detach().numpy(), output['last_hidden_state'][1].detach().numpy())

	sc = cosine_similarity(output["pooler_output"][0].unsqueeze(0).detach().numpy(), output["pooler_output"][1].unsqueeze(0).detach().numpy())

	return sc 

def Evaluate(orig, pred):
	ref_encoding = tokenizer([orig], padding='longest', max_length=20,truncation=True, return_tensors='pt')
	hyp_encoding = tokenizer([pred], padding='longest', max_length=20,truncation=True, return_tensors='pt')

	ref_input_ids, ref_mask = ref_encoding.input_ids, ref_encoding.attention_mask
	hyp_input_ids, hyp_mask = hyp_encoding.input_ids, hyp_encoding.attention_mask

	ref_out = model(input_ids=ref_input_ids, attention_mask=ref_mask)
	hyp_out = model(input_ids=hyp_input_ids, attention_mask=hyp_mask)

	ref_ids = torch.ones([ref_input_ids.shape[0], ref_input_ids.shape[1]])
	hyp_ids = torch.ones([hyp_input_ids.shape[0], hyp_input_ids.shape[1]])
	ref_emb = ref_out["last_hidden_state"]
	hyp_emb = hyp_out["last_hidden_state"]

	P, R,F = greedy_cos_idf(ref_emb, ref_mask, ref_ids, hyp_emb, hyp_mask, hyp_ids)

	return P, R, F 


def removePunc(seq):
	return re.sub(r'[^\w\s]',' ',seq)


def BERTScoreEval(data_path, mode="Concept",direct_path="Direct_Summ.csv",index_path="/Users/ygao/Desktop/MIMIC/Summ-baseline/data/s_a/test.csv", input_baseline=False, write_flag=False):
	ps, rs, fs = [], [], [] 
	data = pd.read_csv(data_path)
	summ_index = list(pd.read_csv(index_path,header=None)[0])
	di_data = pd.read_csv(direct_path)
	pred_data = pd.read_csv(data_path)
	direct_probs = {}
	sent_scs = [] 

	new_outputs = []  # output to csv file 

	if mode == "Direct" or mode == "Indirect":
		for row in range(len(di_data)):
			direct_probs[di_data.iloc[row]["File ID"]] =  di_data.iloc[row]["Summ"]

	ext_all_ents = [] 
	for row in range(len(data)):
		input_data = removePunc(data.iloc[row]["Input"])
		orig = removePunc(data.iloc[row]["Ground Truth"])

		pred = removePunc(data.iloc[row]["Pred"])
		
		if mode == "Concept":
			input_concept = BuildConcepts(matcher, input_data, CUI)
			gt_concepts = BuildConcepts(matcher, orig, CUI)

			in_concept_lists = list(input_concept.keys())
			gt_concepts_lists = list(gt_concepts.keys()) 

			ext_entities = set(in_concept_lists).intersection(set(gt_concepts_lists))

			if len(ext_entities) != 0 :
				ext_all_ents.append(ext_entities)
				pred_concept = BuildConcepts(matcher,pred, CUI)
				if input_baseline:
					pred_concept = input_concept

				p, r, f = Evaluate(" ".join(ext_entities), " ".join(list(pred_concept.keys())))
				sc = SapbertEval(" ".join(ext_entities), " ".join(list(pred_concept.keys())))
				sent_scs.append(sc)
				ps.append(p.item())
				rs.append(r.item())
				fs.append(f.item())

				if not input_baseline:
					new_outputs.append([" ".join(ext_entities), " ".join(list(pred_concept.keys()))])

		elif mode == "Direct" or mode == "Indirect":
			pred = removePunc(pred_data.iloc[row]["Pred"].lower())
			if summ_index[row] not in direct_probs:
				continue 
			di_summs = removePunc(direct_probs[summ_index[row]].lower())
			if input_baseline:
				pred = input_data
			p, r, f = Evaluate(di_summs, pred)
			ps.append(p.item())
			rs.append(r.item())
			fs.append(f.item())
			sc = SapbertEval(di_summs, pred)
			sent_scs.append(sc)

			if not input_baseline:
				new_outputs.append([di_summs, pred]) 

		else:
			if input_baseline:
				pred = input_data
			p, r, f = Evaluate(orig, pred)
			ps.append(p.item())
			rs.append(r.item())
			fs.append(f.item())
			sc = SapbertEval(orig, pred)
			sent_scs.append(sc)

			if not input_baseline:
				new_outputs.append([orig, pred])

	print("BERTScore Mode {}:\t Precision {:.4f}\t & Recall {:.4f}\t & F-measure {:.4f}\t".format(mode, np.mean(ps), np.mean(rs), np.mean(fs)))
	print("SapBERT Sentence Similarity {:.4f}".format(np.mean(sent_scs)))

	if not input_baseline or write_flag:
		out_name = "ForCUI/"+fn.split("/")[-1].split(".")[0] + "_"+mode+".csv" 
		with open(out_name,"w") as outf:
			header = ["Reference", "Prediction"]
			wr = csv.writer(outf)
			wr.writerow(header)
			for l in new_outputs:
				wr.writerow(l)


#def DirectCUI():


def AllCUI(data_path, input_baseline=False,concept_mode=False):
	data = pd.read_csv(data_path)
	orig_cuis = [] 
	pred_cuis = [] 
	for row in range(len(data)):
		input_data = removePunc(data.iloc[row]["Input"])
		orig = removePunc(data.iloc[row]["Ground Truth"])
		pred = removePunc(data.iloc[row]["Pred"])
		if input_baseline:
			pred = input_data
		orig_list = BuildConcepts(matcher, orig, CUI)
		pred_list = BuildConcepts(matcher, pred, CUI)

		if concept_mode:
			input_concept = BuildConcepts(matcher, input_data,CUI) 
			in_concept_lists = list(input_concept.keys())
			gt_concepts_lists = list(orig_list.keys()) 

			ext_entities = set(in_concept_lists).intersection(set(gt_concepts_lists))

			for k,v in orig_list.items():
				if k in ext_entities:
					orig_cuis.extend(orig_list[k]['cui'])

		else:
			for k,v in orig_list.items():
				orig_cuis.extend(orig_list[k]['cui'])

		for k,v in pred_list.items():
			pred_cuis.extend(pred_list[k]['cui'])

		#print(pred_cuis)

	precision = len(set(orig_cuis).intersection(set(pred_cuis))) / len(set(orig_cuis))
	recall = len(set(orig_cuis).intersection(set(pred_cuis))) / len(set(pred_cuis)) 

	f = 2 * (precision * recall) / (precision + recall) 

	print("F-score: {:.2f}".format(f*100)) 


def DirectCUI(data_path, index_path, direct_path, input_baseline=False):
	data = pd.read_csv(data_path)
	summ_index = list(pd.read_csv(index_path,header=None)[0])
	di_data = pd.read_csv(direct_path)
	pred_data = pd.read_csv(data_path)
	direct_probs = {}
	sent_scs = [] 

	new_outputs = []  # output to csv file 
	orig_cuis = [] 
	pred_cuis = [] 

	for row in range(len(di_data)):
		direct_probs[di_data.iloc[row]["File ID"]] =  di_data.iloc[row]["Summ"]

	for row in range(len(data)):

		#pred = removePunc(pred_data.iloc[row]["Pred"].lower())
		if summ_index[row] not in direct_probs:
			continue 
		orig = removePunc(direct_probs[summ_index[row]].lower())

		input_data = removePunc(data.iloc[row]["Input"])
		pred = removePunc(data.iloc[row]["Pred"])
		if input_baseline:
			pred = input_data
		orig_list = BuildConcepts(matcher, orig, CUI)
		pred_list = BuildConcepts(matcher, pred, CUI)

		for k,v in orig_list.items():
			orig_cuis.extend(orig_list[k]['cui'])

		for k,v in pred_list.items():
			pred_cuis.extend(pred_list[k]['cui'])

		#print(pred_cuis)

	precision = len(set(orig_cuis).intersection(set(pred_cuis))) / len(set(orig_cuis))
	recall = len(set(orig_cuis).intersection(set(pred_cuis))) / len(set(pred_cuis)) 

	f = 2 * (precision * recall) / (precision + recall) 

	print("F-score: {:.2f}".format(f*100)) 


def IndirectCUI(data_path, input_baseline=False ):
	data = pd.read_csv(data_path)
	orig_cuis = [] 
	pred_cuis = [] 
	for row in range(len(data)):
		input_data = removePunc(data.iloc[row]["Input"])
		orig = removePunc(data.iloc[row]["Ground Truth"])
		pred = removePunc(data.iloc[row]["Pred"])
		if input_baseline:
			pred = input_data
		orig_list = BuildConcepts(matcher, orig, CUI)
		pred_list = BuildConcepts(matcher, pred, CUI)

		for k,v in orig_list.items():
			orig_cuis.extend(orig_list[k]['cui'])

		for k,v in pred_list.items():
			pred_cuis.extend(pred_list[k]['cui'])

		#print(pred_cuis)

	precision = len(set(orig_cuis).intersection(set(pred_cuis))) / len(set(orig_cuis))
	recall = len(set(orig_cuis).intersection(set(pred_cuis))) / len(set(pred_cuis)) 

	f = 2 * (precision * recall) / (precision + recall) 

	print("F-score: {:.2f}".format(f*100)) 


import glob
fs = ['/Users/ygao/Desktop/MIMIC/Summ-baseline/results/T5-A-ONLY_28-03-22-16-52-test_output.csv', '/Users/ygao/Desktop/MIMIC/Summ-baseline/results/T5_AUG_SA_05-04-22-15-04_test.csv']

#for fn in list(glob.iglob("/Users/ygao/Desktop/MIMIC/Summ-baseline/results/domain_adapt/*.csv")):
#for fn in fs: 
#for fn in ["/Users/ygao/Desktop/MIMIC/Summ-baseline/results/domain_adapt/t5_DAPT_Aug_A_test_output.csv"]:

index_path="/Users/ygao/Desktop/MIMIC/Summ-baseline/data/s_a/test.csv"

ft_fs = list(glob.iglob("/Users/ygao/Desktop/MIMIC/Summ-baseline/results/*.csv")) 
dapt_fs = list(glob.iglob("/Users/ygao/Desktop/MIMIC/Summ-baseline/results/domain_adapt/*.csv"))
rand_tks = list(glob.iglob("/Users/ygao/Desktop/MIMIC/Summ-baseline/results/domain_adapt/randtks/*.csv")) 
#for fn in list(glob.iglob("/Users/ygao/Desktop/MIMIC/Summ-baseline/results/domain_adapt/*.csv")):
for fn in dapt_fs:
	print("===== Currently Processing Results File: {} ==== ".format(fn))
	"""
	print("Concept:")
	AllCUI(fn,input_baseline=False,concept_mode=True)
	print("Direct:")
	DirectCUI(fn,index_path, "Direct_summ.csv",input_baseline=False)
	print("Indirect:")
	DirectCUI(fn,index_path, "Indirect_summ.csv",input_baseline=False)
	print("All:")
	AllCUI(fn,input_baseline=False)
	"""
	BERTScoreEval(fn, mode="Concept",write_flag=False)
	BERTScoreEval(fn, mode="Direct",write_flag=False)
	BERTScoreEval(fn, mode="Indirect", direct_path="Indirect_summ.csv",write_flag=False)
	BERTScoreEval(fn, mode="All",write_flag=False)
"""
for fn in [fs[0]]:
	print("Currently Processing Results File: {}".format(fn))
	BERTScoreEval(fn, mode="Concept", input_baseline=True)
	BERTScoreEval(fn, mode="Direct",input_baseline=True)
	BERTScoreEval(fn, mode="Indirect", direct_path="Indirect_summ.csv",input_baseline=True)
	BERTScoreEval(fn, mode="All",input_baseline=True)

"""







