from sacrerouge.metrics import Rouge 
#from rouge_score import rouge_scorer

"""
Applying SacreROUGE (ROUGE-L)  
"""
def applySacreRouge(gts, preds):
		metric_l = Rouge(compute_rouge_l=True)
		scores = metric_l.score(preds, [gts])

		return scores 
    
def ComputeScROUGE(data):
    """
    Input: data: a Pandas dataframe with two columns: Ground Truth and Prediction
    No need to tokenize 
    """
    scores = [] 
    for i in range(len(data)):
        gt = data.iloc[i]['Ground Truth'] 
        pred = data.iloc[i]['Prediction']
        sc = applySacreRouge(gt, pred)
        scores.append(sc['rouge-l']['f1'])
    return scores 
