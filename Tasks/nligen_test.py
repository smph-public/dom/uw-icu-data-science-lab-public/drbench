"""
Author: ygao
"""
from nligen_dataset import *
from transformers import BertForSequenceClassification, AdamW
from transformers import AutoTokenizer, T5ForConditionalGeneration
from ignite.metrics import Rouge
import time
from tqdm import tqdm 


model_base_path = "t5-base"

pretrained_state_dict = "../Finetune/t5-Path-RandTKs-MedNLI/t5.pth"

tokenizer = AutoTokenizer.from_pretrained(model_base_path)

tokenizer.add_tokens("<PREMISE>")
tokenizer.add_tokens("<HYPOTHESIS>")

data_path = "../Data/Reasoning/"
#data_path = c
#device = torch.device("cpu") # cpu or cuda 
device = torch.device("cuda")

model = T5ForConditionalGeneration.from_pretrained(model_base_path)

model.to(device)
EPOCHS = 10 

model.resize_token_embeddings(len(tokenizer))
model.load_state_dict(torch.load(pretrained_state_dict))
#model.resize_token_embeddings(len(tokenizer))

train_df = pd.read_csv(data_path+"mednli/train.csv")
val_df = pd.read_csv(data_path+"mednli/dev.csv")
test_df = pd.read_csv(data_path+"mednli/test.csv") 

mnli_dataset = MNLIDataGen(train_df, val_df, test_df, tokenizer) 
train_loader, val_loader, test_loader = mnli_dataset.get_data_loaders(batch_size=8)

param_optimizer = list(model.named_parameters())
no_decay = ['bias', 'gamma', 'beta']
optimizer_grouped_parameters = [
	{'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
	 'weight_decay_rate': 0.01},
	{'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
	 'weight_decay_rate': 0.0}
]
lr = 1e-5 
optimizer = AdamW(optimizer_grouped_parameters, lr=lr, correct_bias=False)

print("RUNNING TEST ON THE FINETUNED MODEL")

model.eval()
total_test_acc  = 0
start = time.time()

with torch.no_grad():
	#for batch_idx, (pair_token_ids, mask_ids, y) in enumerate(val_loader):
	for batch in tqdm(test_loader):
		pair_token_ids, mask_ids, y = batch  
		optimizer.zero_grad()
		pair_token_ids = pair_token_ids.to(device)
		mask_ids = mask_ids.to(device)
		labels = y.to(device)

		val_preds = [] 
		gt_labels = [] 
		outputs = model.generate(pair_token_ids)
		#scs = 0
		for b in range(0,y.shape[0]):
			pred_labels = tokenizer.decode(outputs[b], skip_special_tokens=True)
			#val_preds.append(pred_labels)
			gt_labels = tokenizer.decode(labels[b], skip_special_tokens=True)
			#sc = get_nli_acc_rouge(pred_labels, gt_labels)
			if pred_labels == gt_labels:
				#scs += 1  
				total_test_acc += 1	
	
test_acc  = total_test_acc/(len(test_loader) * 8)
end = time.time()
hours, rem = divmod(end-start, 3600)
minutes, seconds = divmod(rem, 60)

print(f'TEST ACC: {total_test_acc:.4f}')
print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
