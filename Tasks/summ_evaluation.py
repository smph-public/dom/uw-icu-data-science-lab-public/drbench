"""
Author: ygao
"""
from ignite import metrics

class Evaluator(object):
	def __init__(self, setting):
		# if using rouge, bert score 
		self.setting = setting
		self.rouge_version = setting.get("rouge_version",["L",2]) # list of rouge versions, e.g. ["L", 2] 
		self.bertscore_flag = setting.get("use_bertscore",False) # not implemented yet 
		self.verbose = setting.get("verbose", False) # if return detailed R/P/F, set as false only return F score

	def applyRouge(self, gts, preds):
		"""
		Input:
		gts : strings
		preds: strings 
		"""

		m = metrics.Rouge(variants=self.rouge_version)
		candidate = preds.lower().split() # rouge is case-sensitive 
		reference = [gts.lower().split()]
		m.update(([candidate], [reference]))

		scores = m.compute() 
		return scores 

	def applyBERTScore(self, gts, preds):
		pass 



