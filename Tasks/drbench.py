"""
Last update: August 18th, adding drbench header; summarizer trainer 
Author: ygao 
TODO: Add SOAP labeler from Brihat
"""
import os 
from transformers import AutoTokenizer, T5ForConditionalGeneration, AdamW
import torch 
import time
import argparse 
import logging 
import datetime 
import torch.nn as nn
from emrqagen_trainer import EMRQA
from medqagen_trainer import MEDQA 
from apgen_trainer import AP
from nligen_trainer import MEDNLI
from summ_trainer import Summarizer  
from summ_evaluation import *
from tabulate import tabulate 

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser(description="DRBENCH(Generation) Pipeline Fine-tuning Config")

	parser.add_argument('--pretrain_name', dest="inputprefix",type=str,default="T5-Base-Vanilla")
	parser.add_argument('--model_path', dest="model_base_path", type=str, 
					default="t5-base", 
					help="Model Base Path. Example: t5-base ")
	parser.add_argument('--pretrain_state_dict', dest="pretrained",type=str,
					default="", 
					help="Path to checkpoints. Default is None.")
	parser.add_argument('--tokenizer_path', dest="tokenizer_path", type=str, default="t5-base")
	parser.add_argument('--data_path', dest="data_base_path", type=str, default="../Data/Reasoning/")
	parser.add_argument('--device', dest="device", type=str, default="cuda")
	parser.add_argument('--epoch',dest="epoch",type=int,default=8)
	parser.add_argument('-i', '--item', action='store', dest='alist',
                    type=str, nargs='*', default=['mednli', 'ap', 'medqa', 'summ'],
                    help="Enter the DRBENCH Tasks. Examples: -i mednli ap -i medqa")
	parser.add_argument('--nli_lr', action='store', dest='nli_lr',
                    type=float, default=1e-5,
                    help="Learning rate for MedNLI")
	parser.add_argument('--medqa_lr', action='store', dest='medqa_lr',
                    type=float, default=1e-5,
                    help="Learning rate for MedQA")
	parser.add_argument('--ap_lr', action='store', dest='ap_lr',
                    type=float, default=1e-5,
                    help="Learning rate for AP")
	parser.add_argument('--summ_lr', action='store', dest='summ_lr', 
					type=float, default=1e-5, 
					help="Learning rate for Problem Summarization") 
	parser.add_argument('--emrqa_lr', action='store', dest='emrqa_lr',
                    type=float, default=1e-5,
                    help="Learning rate for emrQA")
	parser.add_argument("--emrqa_type", action='store', dest='source_types',
                    type=str, nargs='*', default=['risk', "medication", "obesity", "relations", "smoking"],
                    help="Examples: --emrqa_type risk medication")

	args = parser.parse_args() 

	exp_name = args.inputprefix
	model_base_path = args.model_base_path 
	tokenizer_path = args.tokenizer_path
	data_base_path = args.data_base_path 
	device_name = args.device
	EPOCHS  = args.epoch 
	pretrained_state_dict = args.pretrained 
	tasks = args.alist
	nli_lr = args.nli_lr
	medqa_lr = args.medqa_lr
	ap_lr = args.ap_lr
	summ_lr = args.summ_lr,
	emrqa_lr = args.emrqa_lr
	emrqa_source_types = args.source_types


	print("************************************************************")
	print("*                      DRBENCH                             *")
	print("*     Diagnostic Reasoning Benchmark for Clinical NLP      *")
	print("*               ICU Data Science Lab                       *")
	print("*         School of Medicine and Public Health             *")
	print("*            University of Wisconsin-Madison               *")
	print("************************************************************")
	print("\n\n")
	print(f"RUNNING DRBENCH PIPELINE ON TASKS : {tasks}")

	device = torch.device(device_name)

	exp_prefix = datetime.datetime.now().strftime('%d-%m-%y-%H-%M') 
	finetune_path = "../Finetune/"+exp_name+"-DRBENCH"
	finetune_path += "-".join(exp_prefix.split("-")[:3])+"/"

	if os.path.isdir(finetune_path):
		pass 
	else:
		os.makedirs(finetune_path)
	tokenizer = AutoTokenizer.from_pretrained(model_base_path, use_fast=False)

	model = T5ForConditionalGeneration.from_pretrained(model_base_path)

	if pretrained_state_dict != "":
		model.load_state_dict(torch.load(pretrained_state_dict))

	logging.basicConfig(filename=finetune_path+"train.log", level=logging.INFO)
	exp_setting = f"Exp Name: {exp_name}\t Model Base Path: {model_base_path}\t Pretrained State Dict: {pretrained_state_dict}\t \n"
	exp_params = [["Summ", summ_lr], ["NLI", nli_lr], ["MedQA", medqa_lr], ['AP', ap_lr], ['EMRQA', emrqa_lr]] 
	headers = ["Task", "Learning Rate"]
	exp_params_print = tabulate(exp_params, headers=headers)
	#print(exp_params_print)
	exp_setting += exp_params_print+"\n"
	exp_setting += f"End-to-end Tasks Selected: {tasks}\t Epochs: {EPOCHS}\t Pretrained Type: {exp_name}"

	now = datetime.datetime.now()
	dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
	print("TRAINER CREATED: date and time =", dt_string)	
	print(f"DRBENCH PIPELINE SETUP: {exp_setting}")
	logging.info("TRAINER CREATED: date and time = {}".format(str(dt_string))) 
	logging.info(exp_setting) 

	if "mednli" in tasks:
		task_report = "======= FINETUNING FOR MEDNLI ======="
		print(task_report)
		logging.info(task_report)
		data_path = data_base_path
		bot = MEDNLI(model, tokenizer, data_path, nli_lr, EPOCHS, device, exp_name, finetune_path)
		best_model = bot.train()
		bot.test(best_model)

		del bot # delete the class object to save spaces 

	if "emrqa" in tasks:
		task_report = "======= FINETUNING FOR EMRQA ======="
		print(task_report)
		logging.info(task_report)
		data_path = data_base_path+"emrQA/"
		for stype in emrqa_source_types:
			print(f"== SOURCE TYPE: {stype} == ")
			logging.info(f"==  SOURCE TYPE: {stype} == ")
			bot = EMRQA(model, tokenizer, data_path, emrqa_lr, EPOCHS, device, exp_name, finetune_path, stype)
			best_model = bot.train()
			bot.test(best_model)
		del bot 

	if "medqa" in tasks:
		"""
		Closed Book MEDQA 
		"""
		open_book = False 
		task_report = "======= FINETUNING FOR MEDQA Closed Book ======="
		print(task_report)
		logging.info(task_report)
		data_path = data_base_path+"MedQA/questions/"
		bot = MEDQA(model, tokenizer, data_path, medqa_lr, EPOCHS, device, exp_name, finetune_path, open_book=open_book)
		best_model = bot.train()
		bot.test(best_model)
		del bot 

		"""
		Open Book MEDQA 
		"""
		open_book = True 
		task_report = "======= FINETUNING FOR MEDQA Open Book ======="
		print(task_report)
		logging.info(task_report)
		data_path = data_base_path+"MedQA/questions/"
		bot = MEDQA(model, tokenizer, data_path, medqa_lr, EPOCHS, device, exp_name, finetune_path, open_book=open_book)
		best_model = bot.train()
		bot.test(best_model)
		del bot 


	if "ap" in tasks:
		task_report = f"======= FINETUNING FOR AP ======="
		data_path = data_base_path+"AP/"
		print(task_report)
		logging.info(task_report)
		bot = AP(model, tokenizer, data_path, ap_lr, EPOCHS, device, exp_name, finetune_path)
		best_model = bot.train()
		bot.test(best_model)
		del bot 

	if "summ" in tasks:
		task_report = f"======= FINETUNING FOR Summarization ======="
		data_path = data_base_path+"Summ/"
		print(task_report)
		logging.info(task_report)
		configs = {"LR": summ_lr, 
				"epoch": EPOCHS,
				"device": device_name, 
				"batch_size":4,
				}
		setting = {} 
		evaluator = Evaluator(setting)
		bot = Summarizer(model, tokenizer, evaluator, data_path, configs, exp_name, finetune_path)
		best_model = bot.train()
		pred_seqs, gold_seqs = bot.test(best_model)
		del bot 





