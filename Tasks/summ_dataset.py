"""
Author: ygao
"""
import re
import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import pickle
import os
import pandas as pd 

class NoteSummary(Dataset):
	def __init__(self, train_df, val_df, test_df, tokenizer, task_prefix, summ_type):
		#self.raw_data = data.dropna(subset=[1],inplace=True)
		#self.raw_data = data 
		self.train_df = train_df
		self.val_df = val_df
		self.test_df = test_df
		self.tokenizer = tokenizer # here will be pre-trained tokenizer 
		self.prefix = task_prefix #"MEDGENERATION "
		self.summ_type = summ_type
		self.init_data()


	def init_data(self):
		self.train_data = self.load_data(self.train_df)
		self.val_data = self.load_data(self.val_df)
		self.test_data = self.load_data(self.test_df) 

	def load_data(self, df):
		#print("Current IDX: ", idx)
		self.max_source_length = 512
		self.max_target_length = 64 

		data = pd.read_csv(df)
		data.dropna(inplace=True)

		src_ids = []
		src_masks = []
		tgt_ids = []
		tgt_masks = []

		fids = data["File ID"].tolist()
		srcs = data["Assessment"].tolist()
		tgts = data["Summary"].tolist()
		#sos = data["SO"].tolist() # Added August 21: adding Subjective and Objective sections 
		ss = data['S'].tolist()
		os = data['O'].tolist()

		for (fid, src, gts, s, o) in zip(fids, srcs, tgts, ss, os):
			tgt = re.sub('\W+',' ',gts) 
			#noteid = self.fids[idx]
			#gts = self.tgts[idx]
			#tgt = re.sub('\W+',' ',gts)
			if self.summ_type == "All":
				input_str = self.prefix + " <ASSESSMENT> "+ src + " <SUBJECTIVE> "+ s +" <OBJECTIVE> " + o
			elif self.summ_type == "S+A":
				input_str = self.prefix + " <ASSESSMENT> "+ src + " <SUBJECTIVE> "+ s 
			else:
				input_str = self.prefix + " <ASSESSMENT> "+ src 
			encoding = self.tokenizer(
			    			input_str,
			    			add_special_tokens=True, 
						    padding="max_length",
						    max_length=self.max_source_length,
						    truncation=True,
						    return_tensors="pt",
								)
			input_ids, attention_mask = encoding["input_ids"], encoding["attention_mask"]

			# encode the targets
			target_encoding = self.tokenizer(
			    			tgt, 
			    			add_special_tokens=True, 
			    			padding="max_length", 
			    			max_length=self.max_target_length,
			    			truncation=True,
			    			return_tensors="pt",
										)
			labels, target_mask = target_encoding["input_ids"], target_encoding["attention_mask"]
			src_ids.append(input_ids)
			src_masks.append(attention_mask)
			tgt_ids.append(labels)
			tgt_masks.append(target_mask)

		src_ids = pad_sequence(src_ids, batch_first=True)
		src_masks = pad_sequence(src_masks, batch_first=True)
		tgt_ids = pad_sequence(tgt_ids, batch_first=True)
		tgt_masks = pad_sequence(tgt_masks, batch_first=True)
		dataset = TensorDataset(src_ids, src_masks, tgt_ids,tgt_masks)
		print(len(dataset))
		return dataset


	def get_data_loaders(self, batch_size=32, shuffle=True):
		train_loader = DataLoader(
			self.train_data,
			shuffle=shuffle,
			batch_size=batch_size
		)

		val_loader = DataLoader(
			self.val_data,
			shuffle=shuffle,
			batch_size=batch_size
		)

		test_loader = DataLoader(
			self.test_data,
			shuffle=False,
			batch_size=1)

		return train_loader, val_loader, test_loader


if __name__ == "__main__":
	from transformers import AutoTokenizer
	tok = AutoTokenizer.from_pretrained("t5-base")
	
	train_df = "../Data/Reasoning/Summ/train.csv"
	val_df = "../Data/Reasoning/Summ/val.csv"
	test_df = "../Data/Reasoning/Summ/test.csv"

	dataset = NoteSummary(train_df, val_df, test_df, tok, "summarize: ")
	train_data, val_data, test_data = dataset.get_data_loaders(batch_size=1)
