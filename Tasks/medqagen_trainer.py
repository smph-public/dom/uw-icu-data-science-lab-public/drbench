"""
Author: ygao
"""
import os 
from medqagen_dataset import *
from transformers import AutoTokenizer, T5ForConditionalGeneration, AdamW
import time
from tqdm import tqdm
import argparse 
#from evaluation import Evaluator 
import logging 
import datetime 
import csv
import torch.nn as nn


class MEDQA(nn.Module):
	def __init__(self, model, tokenizer, data_base_path, lr, epochs, device, exp_name, finetune_path, open_book=False, run_test=True):
		super(MEDQA, self).__init__()
		self.model = model 
		self.tokenizer = tokenizer
		self.data_base_path = data_base_path
		self.device = device
		self.exp_name = exp_name
		self.ft_path = finetune_path
		self.run_test = run_test
		self.need_prefix = True
		self.open_book = open_book
		self.prefix = "CLINICAL QA:"  
		self.beam_size=5
		self.lr = lr 
		self.epoch = epochs
		self.init_dataset()
		self.create_optimizer(lr)


	def init_dataset(self, batch_size=8):
		need_prefix=True
		prefix = "CLINICAL QA: "
		self.tokenizer.add_tokens(prefix) 
		self.tokenizer.add_tokens("<A>")
		self.tokenizer.add_tokens("<B>")
		self.tokenizer.add_tokens("<C>")
		self.tokenizer.add_tokens("<D>")
		self.tokenizer.add_tokens("<E>")
		self.tokenizer.add_tokens("<QUESTION>")

		if self.open_book:
			self.tokenizer.add_tokens("<CONTEXT>")

		self.model.resize_token_embeddings(len(self.tokenizer))

		medqa_dataset = MEDQAGen(self.data_base_path+"train.jsonl", 
									self.data_base_path+"dev.jsonl", 
									self.data_base_path+"test.jsonl", 
									self.tokenizer, 
									data_base_path=self.data_base_path, 
									open_book=self.open_book, 
									need_prefix=self.need_prefix, 
									prefix=prefix)
		self.train_loader, self.val_loader, self.test_loader = medqa_dataset.get_data_loaders(batch_size=8)

		self.model.resize_token_embeddings(len(self.tokenizer)) 
		self.model.to(self.device)
		logging.info(f"==== TASKS: MEDQA ==== \n Data Base Path: {self.data_base_path}, Exp Name: {self.exp_name}, Open book(T/F): {self.open_book}, LR: {self.lr}")

	def create_optimizer(self, lr):
		param_optimizer = list(self.model.named_parameters())
		no_decay = ['bias', 'gamma', 'beta']
		optimizer_grouped_parameters = [
			{'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
			 'weight_decay_rate': 0.01},
			{'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
			 'weight_decay_rate': 0.0}
			]

		self.optimizer = AdamW(optimizer_grouped_parameters, lr=lr, correct_bias=False)

	def train(self):

		total_step = len(self.train_loader)

		min_loss = 100 

		best_model = None 
		for epoch in range(self.epoch):
			start = time.time()
			self.model.train()
			total_train_loss = 0
			total_train_acc  = 0
			batch_cnt = 0

			for batch in tqdm(self.train_loader, unit="batch",desc="Training"):
				#batch_cnt +=1
				#if batch_cnt > 2:
				#	continue

				source_ids, mask_ids, target_ids, target_masks = batch
				self.optimizer.zero_grad()
				source_ids = source_ids.to(self.device)
				mask_ids = mask_ids.to(self.device)
				labels = target_ids.to(self.device)
				tgt_mask = target_masks.to(self.device)

				labels[labels[:, :] == self.tokenizer.pad_token_id] = -100

				outputs = self.model(
					input_ids=source_ids, 
					attention_mask=mask_ids,
					labels = labels,
					decoder_attention_mask=tgt_mask
				)

				loss = outputs[0]

				loss.backward()
				self.optimizer.step()

				total_train_loss += loss.item()
				train_loss = total_train_loss/len(self.train_loader)

			self.model.eval()
			total_val_acc  = 0
			total_val_loss = 0
			val_batch_cnt = 0
			with torch.no_grad():
				for batch in tqdm(self.val_loader, unit="batch",desc="Validating"):
					source_ids, mask_ids, target_ids, target_masks = batch
					self.optimizer.zero_grad()
					#val_batch_cnt +=1 
					#if val_batch_cnt > 2:
					#	continue 

					source_ids = source_ids.to(self.device)
					mask_ids = mask_ids.to(self.device)
					labels = target_ids.to(self.device)
					tgt_mask = target_masks.to(self.device)
					labels[labels[:, :] == self.tokenizer.pad_token_id] = -100

					outputs = self.model(
						input_ids=source_ids, 
						attention_mask=mask_ids,
						labels = labels,
						decoder_attention_mask=tgt_mask
					)

					loss = outputs[0]
					generate_ids = self.model.generate(input_ids=source_ids,        
										attention_mask=mask_ids,
										max_length=10,
										num_beams=1,
										repetition_penalty=1,
										no_repeat_ngram_size=2)

					for b in range(0,labels.shape[0]):
						pred_labels = self.tokenizer.decode(generate_ids[b], skip_special_tokens=True)
						gt_labels = self.tokenizer.decode(target_ids[b], skip_special_tokens=True)
						if pred_labels == gt_labels:
							#scs += 1  
							total_val_acc += 1

					total_val_loss += loss.item()

				val_acc  = total_val_acc/(len(self.val_loader)*8)
				val_loss = total_val_loss/(len(self.val_loader)*8)
			end = time.time()
			hours, rem = divmod(end-start, 3600)
			minutes, seconds = divmod(rem, 60)

			train_report = f'Epoch {epoch+1}: train_loss: {train_loss:.4f}  | val_loss: {val_loss:.4f} val_acc: {val_acc:.4f}'
			print(train_report)
			logging.info(train_report)
			val_report = f"Total Accurate Samples in Validation: {total_val_acc}"
			print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
			logging.info(val_report)

			if val_loss < min_loss:
				min_loss = val_loss
				best_model = self.model

			if val_loss > min_loss:
				break 

		return best_model 

	def test(self, best_model):
		total_test_acc = 0
		outname=self.ft_path+"MedQA_"+str(self.open_book)+"_test.csv"
		golds, preds = [], [] 
		with torch.no_grad():
			for batch in tqdm(self.test_loader):
				pair_token_ids, mask_ids, y, y_masks = batch
				pair_token_ids = pair_token_ids.to(self.device)
				mask_ids = mask_ids.to(self.device)
				labels = y.to(self.device)

				outputs = best_model.generate(input_ids=pair_token_ids,                    	
									attention_mask=mask_ids,
									max_length=128,
									num_beams=1,
									repetition_penalty=1,
									no_repeat_ngram_size=2)

				for b in range(0,y.shape[0]):
					pred_labels = self.tokenizer.decode(outputs[b], skip_special_tokens=True)
					gt_labels = self.tokenizer.decode(labels[b], skip_special_tokens=True)
					golds.append(gt_labels)
					preds.append(pred_labels)
					if pred_labels == gt_labels: 
						total_test_acc += 1

			with open(outname,"w") as outf:
				wr = csv.writer(outf)
				header = ["Gold","Pred"]
				wr.writerow(header)

				for l in zip(golds, preds):
					wr.writerow([l[0], l[1]])

		test_acc  = total_test_acc/len(self.test_loader)

		test_report = f"Model {self.exp_name} Test Acc: {test_acc:.4f} Number of Acc Samples: {total_test_acc}"
			
		logging.info(test_report)
		print(test_report)

if __name__ == "__main__":
	
	parser = argparse.ArgumentParser(description="MEDQA Generation Fine-tuning Config")
	#yaml_path = "yamls/t5.yaml" uncomment this line if run under command line

	parser.add_argument('--lr', dest="lr",type=float, default=1e-5)
	parser.add_argument('--pretrain_name', dest="inputprefix",type=str,default="T5-TB-DEF")
	parser.add_argument('--model_path', dest="model_base_path", type=str, default="t5-base")
	parser.add_argument('--pretrain_state_dict', dest="pretrained",type=str,default="../PLM/T5-Def-TB-20-07-22-02-55/t5.pth")
	parser.add_argument('--tokenizer_path', dest="tokenizer_path", type=str, default="t5-base")
	parser.add_argument('--data_path', dest="data_base_path", type=str, default="../Data/Reasoning/MedQA/questions/")
	parser.add_argument('--device', dest="device", type=str, default="cuda")
	parser.add_argument('--epoch',dest="epoch",type=int,default=5)
	parser.add_argument('--open_book',dest="open_book",type=bool,default=False)


	args = parser.parse_args() 

	lr = args.lr 
	exp_name = args.inputprefix
	model_base_path = args.model_base_path 
	tokenizer_path = args.tokenizer_path
	data_base_path = args.data_base_path 
	device_name = args.device
	EPOCHS  = args.epoch 
	open_book = args.open_book 
	pretrained_state_dict = args.pretrained 

	device = torch.device(device_name)

	exp_prefix = datetime.datetime.now().strftime('%d-%m-%y-%H-%M') 
	finetune_path = "../Finetune/"+exp_name+"-MEDQA"

	if open_book:
		finetune_path += "-Open-"+ "-".join(exp_prefix.split("-")[:3])+"/"
	else:
		finetune_path += "-".join(exp_prefix.split("-")[:3])+"/"

	if os.path.isdir(finetune_path):
		pass 
	else:
		os.makedirs(finetune_path)
	tokenizer = AutoTokenizer.from_pretrained(model_base_path, use_fast=False)

	model = T5ForConditionalGeneration.from_pretrained(model_base_path)

	if pretrained_state_dict:
		model.load_state_dict(torch.load(pretrained_state_dict))

	print("Open Book: ", open_book)

	logging.basicConfig(filename=finetune_path+"train.log", level=logging.INFO)
	exp_setting = f"LR: {lr}\t Epochs: {EPOCHS}\t Pretrained Type: {exp_name}"

	now = datetime.datetime.now()
	dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
	print("TRAINER CREATED: date and time =", dt_string)	
	logging.info("TRAINER CREATED: date and time = {}".format(str(dt_string))) 

	logging.info(exp_setting) 
	bot = MEDQA(model, tokenizer, data_base_path, lr, EPOCHS, device, exp_name, finetune_path, open_book=open_book)
	best_model = bot.train()
	bot.test(best_model)
