import os

lr_range = [3e-4, 2e-4, 1e-4, 8e-5, 3e-5, 8e-6, 5e-6]

model_pretrain_dicts = ["../PLM/t5-large-Path-02-08-22-16-24/t5.pth", "../PLM/t5-largeCUI-TB+DEF03-08-22-14-56/pth", "../PLM/t5-largeTB+DEF+Path10-08-22-04-14/t5.pth"]
pretrain_names = ["t5-large-path", "t5-large-CUI-TB+DEF", "t5-large-TB+DEF+PATH"]

for pair in zip(model_pretrain_dicts, pretrain_names):
	for lr in lr_range:
		cmd = f"CUDA_VISIBLE_DEVICES=1 python summ_trainer.py --model_path t5-large --pretrain_name {pair[1]} --pretrain_state_dict {pair[0]} --lr {lr} "
		os.system(cmd) 