"""
Author: ygao
"""
import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import pickle
import os
from transformers import BertTokenizer
import pandas as pd 
import json 

class EMRQAGen(Dataset):
	def __init__(self, data_path, tokenizer, source_type=None,need_prefix=True, prefix="CLINICAL QA: "):

		self.base_path = data_path
		#self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True) # Using a pre-trained BERT tokenizer to encode sentences
		self.tokenizer = tokenizer
		self.source_type = source_type
		if source_type:
			self.train_data = data_path + source_type +"_train.csv"
			self.val_data = data_path + source_type + "_eval.csv"
			self.test_data = data_path + source_type + "_test.csv"
		else:
		    self.train_data = data_path + "data_train.csv"
		    self.val_data = data_path + "data_eval.csv"
		    self.test_data = data_path + "data_test.csv"
		self.need_prefix = need_prefix
		self.prefix = prefix

		self.init_data()

	def init_data(self):

		self.train_data = self.load_data(self.train_data)
		self.val_data = self.load_data(self.val_data)
		self.test_data = self.load_data(self.test_data) 

	def load_data(self, df):
		MAX_LEN = 512
		token_ids = []
		mask_ids = []
		target_masks = []
		y = []
		answer_text = [] 

		data = pd.read_csv(df)
		data.dropna(inplace=True)
		for _, item in data.iterrows():

			question, answer, context = item[0], item[1], item[2]
			input_tks =  question  
			if self.need_prefix:
				input_tks = self.prefix + " <Q> " + question + " <C> "+context 

			seqs_tks = self.tokenizer(input_tks, add_special_tokens=True, max_length=512, truncation=True)
			seqs_ids = torch.tensor(seqs_tks["input_ids"])
			attention_mask_ids = torch.tensor(seqs_tks["attention_mask"])

			answer_tks = self.tokenizer(answer, add_special_tokens=True, max_length=512, truncation=True)
			answer_ids = torch.tensor(answer_tks["input_ids"])
			answer_masks = torch.tensor(answer_tks["attention_mask"])

			token_ids.append(seqs_ids)
			mask_ids.append(attention_mask_ids)

			y.append(answer_ids) 
			target_masks.append(answer_masks)
			answer_text.append(answer)
	
		token_ids = pad_sequence(token_ids, batch_first=True)
		mask_ids = pad_sequence(mask_ids, batch_first=True)
		target_labels = pad_sequence(y, batch_first=True)
		target_masks = pad_sequence(target_masks, batch_first=True)
		#y = torch.tensor(y)
		dataset = TensorDataset(token_ids, mask_ids, target_labels, target_masks)
		if self.source_type:
			print(f"emrQA Type {self.source_type}: {len(dataset)}")
		else:
			print(f"emrQA: {len(dataset)}")
		return dataset


	def get_data_loaders(self, batch_size=32, shuffle=True):
		train_loader = DataLoader(
		  self.train_data,
		  shuffle=shuffle,
		  batch_size=batch_size
		)

		val_loader = DataLoader(
		  self.val_data,
		  shuffle=shuffle,
		  batch_size=batch_size
		)

		test_loader = DataLoader(
		  self.test_data,
		  shuffle=False,
		  batch_size=1 
		)

		return train_loader, val_loader, test_loader
