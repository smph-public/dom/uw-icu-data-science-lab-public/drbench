"""
Author: ygao, jcaskey 
"""
from apgen_dataset import *
from transformers import BertForSequenceClassification, AdamW
from transformers import AutoTokenizer, T5ForConditionalGeneration
# from ignite.metrics import Rouge
import torch
import time
import datetime
from tqdm import tqdm 
import pandas as pd
import torch.nn as nn
import argparse
import logging 
import csv

class AP(nn.Module):
	def __init__(self, model, tokenizer, data_base_path, lr, epochs, device, exp_name, finetune_path, run_test=True):
		super(AP, self).__init__()
		self.model = model 
		self.tokenizer = tokenizer
		self.data_base_path = data_base_path
		self.device = device
		self.exp_name = exp_name
		self.ft_path = finetune_path
		self.run_test = run_test
		self.need_prefix = True
		self.prefix = "INFERENCE: "  # change prefix from 'REASONING: ' to 'INFERENCE: '
		self.beam_size=5
		self.LR = lr 
		self.EPOCHS = epochs
		self.init_dataset()
		self.create_optimizer(lr)


	def init_dataset(self, batch_size=8):
		self.tokenizer.add_tokens(self.prefix)
		self.tokenizer.add_tokens("<ASSESSMENT>")
		self.tokenizer.add_tokens("<PLAN SUBSECTION>")
		self.model.resize_token_embeddings(len(self.tokenizer))

		train_df = pd.read_csv(self.data_base_path+"train.csv")
		val_df = pd.read_csv(self.data_base_path+"dev.csv")
		test_df = pd.read_csv(self.data_base_path+"test.csv") 

		# debug: check for NA
		if train_df.isnull().values.any():
			print('WARNING! train_df has na values!')
			print(train_df[train_df.isna().any(axis=1)])
			print('\nDropping these rows from the dataframe')
			train_df = train_df.dropna()
		if test_df.isnull().values.any():
			print('WARNING! test_df has na values!')
			print(test_df[test_df.isna().any(axis=1)])
			print('\nDropping these rows from the dataframe')
			test_df = test_df.dropna()
		if val_df.isnull().values.any():
			print('WARNING! val_df has na values!')
			print(val_df[val_df.isna().any(axis=1)])
			print('\n Dropping these rows from the dataframe')
			val_df = val_df.dropna()

		# call dataloader 
		a_p_dataset = APGen(train_df, val_df, test_df, self.tokenizer) 
		# modify 08032022 JRC: set batch_size = 8 instead of 4
		self.train_loader, self.val_loader, self.test_loader = a_p_dataset.get_data_loaders(batch_size=8)

		self.model.to(self.device)
		logging.info(f"==== TASKS: AP Reasoning ==== \n Data Base Path: {self.data_base_path}, Exp Name: {self.exp_name}, LR: {self.LR}")

	def create_optimizer(self, LR):
		param_optimizer = list(self.model.named_parameters())
		no_decay = ['bias', 'gamma', 'beta']
		optimizer_grouped_parameters = [
			{'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
			 'weight_decay_rate': 0.01},
			{'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
			 'weight_decay_rate': 0.0}
			]

		self.optimizer = AdamW(optimizer_grouped_parameters, lr=LR, correct_bias=False)

	def train(self):
		min_loss = 100
		stopCt = 0
		for epoch in range(self.EPOCHS):
			start = time.time()
			self.model.train()
			total_train_loss = 0
			total_train_acc  = 0
			batch_cnt = 0 
			for batch in tqdm(self.train_loader):
				#if batch_cnt > 2:
				#	continue 
				batch_cnt +=1 
				pair_token_ids, mask_ids, y = batch 
				self.optimizer.zero_grad()
				pair_token_ids = pair_token_ids.to(self.device)
				mask_ids = mask_ids.to(self.device)
				labels = y.to(self.device)
				loss = self.model(input_ids=pair_token_ids, 
									 attention_mask=mask_ids, 
									 labels=labels)[0]

				loss.backward()
				self.optimizer.step()

				total_train_loss += loss.item()

			train_loss = total_train_loss/len(self.train_loader)

			self.model.eval()
			total_val_acc  = 0
			total_val_loss = 0
			batch_cnt = 0
			with torch.no_grad():
				for batch in tqdm(self.val_loader):
					#if batch_cnt > 2:
					#	continue 
					batch_cnt +=1 

					pair_token_ids, mask_ids, y = batch  
					self.optimizer.zero_grad()
					pair_token_ids = pair_token_ids.to(self.device)
					mask_ids = mask_ids.to(self.device)
					labels = y.to(self.device)

					val_preds = [] 

					outputs = self.model(input_ids=pair_token_ids, 
									attention_mask=mask_ids, 
									labels=labels)
					loss = outputs[0]
					generate_ids = self.model.generate(input_ids=pair_token_ids, 					max_length=128, 
											num_beams=5, 
											repetition_penalty=1, 
											no_repeat_ngram_size=2)
					gt_labels = [] 
					for b in range(0,y.shape[0]):
						pred_labels = self.tokenizer.decode(generate_ids[b], skip_special_tokens=True)
						gt_labels = self.tokenizer.decode(y[b], skip_special_tokens=True)
						if pred_labels == gt_labels:
							total_val_acc += 1

					total_val_loss += loss.item()
				
				
			val_acc  = total_val_acc/(len(self.val_loader) * 8)
			val_loss = total_val_loss/(len(self.val_loader) * 8)
			end = time.time()
			hours, rem = divmod(end-start, 3600)
			minutes, seconds = divmod(rem, 60)

			train_report = f'Epoch {epoch+1}: train_loss: {train_loss:.4f} | val_loss: {val_loss:.4f} val_acc: {val_acc:.4f}\n '
			train_report += "{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds)
			print(train_report)
			logging.info(train_report)

			# modification 08022022 JRC
			if val_loss < min_loss:
				min_loss = val_loss
				best_model = self.model
				stopCt = 0
			if val_loss >= min_loss or val_loss <= 0.001:
				stopCt += 1
				if stopCt > 3:
					print('stopping training because val_loss exceeds or equals min_loss after 3 consecutive loops')
					break

		return best_model

	def test(self, best_model):
		print("RUNNING TEST ON THE FINETUNED MODEL")
		best_model.eval()
		total_test_acc  = 0
		batch_cnt = 0 
		start = time.time()
		golds, preds = [], []
		outname = self.ft_path + "/ap_test.csv"
		with torch.no_grad():
			for batch in tqdm(self.test_loader):
				pair_token_ids, mask_ids, y = batch
				#if batch_cnt > 2:
				#	continue 
				batch_cnt +=1   
				pair_token_ids = pair_token_ids.to(self.device)
				mask_ids = mask_ids.to(self.device)
				labels = y.to(self.device)
				val_preds = [] 
				gt_labels = [] 
				outputs = best_model.generate(input_ids=pair_token_ids, 
									max_length=128, 
									num_beams=5, 
									repetition_penalty=1, 
									no_repeat_ngram_size=2)
				for b in range(0,y.shape[0]):
					pred_labels = self.tokenizer.decode(outputs[b], skip_special_tokens=True)
					gt_labels = self.tokenizer.decode(labels[b], skip_special_tokens=True)
					if pred_labels == gt_labels:
						total_test_acc += 1
					preds.append(pred_labels)
					golds.append(gt_labels)

		with open(outname, "w") as outf:
			wr = csv.writer(outf)
			for _ in zip(golds, preds):
				wr.writerow([_[0], _[1]])
		
		test_acc  = total_test_acc/(len(self.test_loader))
		end = time.time()
		hours, rem = divmod(end-start, 3600)
		minutes, seconds = divmod(rem, 60)

		test_report = f'TEST ACC: {test_acc:.4f}'
		print("{:0>2}:{:0>2}:{:05.2f} TEST ACC: {:.4f}".format(int(hours),int(minutes),seconds, test_acc))
		logging.info(test_report)

		return test_acc


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="A&P Generation Fine-tuning Config")
	parser.add_argument('--lr', dest="lr",type=float, default=1e-5)
	parser.add_argument('--pretrain_name', dest="inputprefix",type=str,default="T5-TB-DEF")
	parser.add_argument('--model_path', dest="model_base_path", type=str, default="t5-base")
	parser.add_argument('--pretrain_state_dict', dest="pretrained",type=str,default="")
	parser.add_argument('--tokenizer_path', dest="tokenizer_path", type=str, default="t5-base")
	parser.add_argument('--data_path', dest="data_base_path", type=str, default="../Data/Reasoning/AP/")
	parser.add_argument('--device', dest="device", type=str, default="cuda")
	parser.add_argument('--epoch',dest="epoch",type=int,default=5)
	parser.add_argument('--run_test', dest='run_test', type=bool, default=True)

	args = parser.parse_args() 

	lr = args.lr 
	exp_name = args.inputprefix
	model_base_path = args.model_base_path 
	tokenizer_path = args.tokenizer_path
	data_base_path = args.data_base_path 
	device_name = args.device
	EPOCHS  = args.epoch 
	pretrained_state_dict = args.pretrained 
	run_test = args.run_test 

	device = torch.device(device_name)

	exp_prefix = datetime.datetime.now().strftime('%d-%m-%y-%H-%M') 
	finetune_path = "../Finetune/"+exp_name+"-AP"

	finetune_path += "-".join(exp_prefix.split("-")[:3])+"/"
	if os.path.isdir(finetune_path):
		pass 
	else:
		os.makedirs(finetune_path)
	tokenizer = AutoTokenizer.from_pretrained(model_base_path, use_fast=False)

	model = T5ForConditionalGeneration.from_pretrained(model_base_path)

	if pretrained_state_dict != "":
		model.load_state_dict(torch.load(pretrained_state_dict))

	logging.basicConfig(filename=finetune_path+"train.log", level=logging.INFO)
	exp_setting = f"LR: {lr}\t Epochs: {EPOCHS}\t Pretrained Type: {exp_name}"

	now = datetime.datetime.now()
	dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
	print("TRAINER CREATED: date and time =", dt_string)	
	logging.info("TRAINER CREATED: date and time = {}".format(str(dt_string))) 

	logging.info(exp_setting) 
	bot = AP(model, tokenizer, data_base_path, lr, EPOCHS, device, exp_name, finetune_path,run_test=run_test)
	best_model = bot.train()
	if run_test:
		bot.test(best_model)









